commodore-scripts
=================

`commodore-scripts` repository provides a bunch of auxiliary [Perl](http://www.perl.org/) scripts, [Makefile](https://www.gnu.org/software/make/) files, as well as ready-to-use [MOS 6510](http://en.wikipedia.org/wiki/MOS_Technology_6510) assembly source code snippets with a purpose of simplifying and speeding up development of new Commodore 64 productions. See individual section's `README` files for a detailed information on a particular helper files contained within this repository.

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2014-2023 by Pawel Krol.

See specific repository sections (in appropriate `README` files) for more detailed information about licensing schemes, since they might differ for individual components.

Unless specified otherwise, the software is distributed under the terms of the MIT license. See [LICENSE](https://bitbucket.org/pawelkrol/commodore-scripts/src/master/LICENSE.md) for more information.
