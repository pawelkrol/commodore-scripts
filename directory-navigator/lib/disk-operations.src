;----------------------------------
; ** ENTER (FILE) NAME COMPONENT **
;----------------------------------
; INPUT PARAMETERS:
;  * auto_suffix
;  * auto_suffix_length
;  * min_length
;  * max_length
;----------------------------------
EDIT_FIELD_DRAW_BORDER_ROW    = (IDE64_LIST_FIRST_SELECTION_ROW - 1) + (IDE64_LIST_MAX_VISIBLE_ITEMS_ON_PAGE >> 1)
EDIT_FIELD_DRAW_BORDER_SCREEN = IDE64_LIST_SCREEN + EDIT_FIELD_DRAW_BORDER_ROW * $28
EDIT_FIELD_DRAW_BORDER_COLORS = EDIT_FIELD_DRAW_BORDER_SCREEN + ($d800 - IDE64_LIST_SCREEN)
;----------------------------------
enter_name_initialise_rename_dir

                jsr enter_name_initialise_input_field_size

                jsr enter_name_initialise_edited_text_field

                jmp enter_name_initialise_data
;----------------------------------
enter_name_initialise_create_dir

                jsr enter_name_initialise_input_field_size

                jsr enter_name_initialise_empty_text_field

                jmp enter_name_initialise_data
;----------------------------------
enter_name_initialise_input_field_size

                ; text_width = max_length + auto_suffix_length
                lda enter_name_max_length
                clc
                adc enter_name_auto_suffix_length
                sta enter_name_text_width

                ; full_width = text_width + 2
                clc
                adc #$02
                sta enter_name_full_width

                rts
;----------------------------------
enter_name_initialise_remove_dir

                lda #ENTER_NAME_ARE_YOU_SURE_Y_N_LENGTH
                sta enter_name_text_width

                lda #(ENTER_NAME_ARE_YOU_SURE_Y_N_LENGTH + 2)
                sta enter_name_full_width

                jsr enter_name_initialise_empty_text_field

                jmp enter_name_initialise_data
;----------------------------------
enter_name_initialise_empty_text_field

                ; initial input field text length
                lda #$00
                sta enter_name_file_name_length

                rts
;----------------------------------
enter_name_initialise_edited_text_field

                ; set vector to highlighted directory name
                jsr set_fb_fc_vector_to_current_item_name
                jsr dir_navigator_inc_vec_fb_fc ; skip opening quotation mark

                ; initial input field text content
                ldy #$00
                lda ($fb),y
                cmp #$22 ; '"'
                beq *+9
                sta enter_name_file_name_text,y
                iny
                jmp *-10

                ; initial input field text length
                sty enter_name_file_name_length

                rts
;----------------------------------
enter_name_initialise_data

                ; initial cursor X = border left offset X + 1
                jsr enter_name_get_draw_border_offset_x
                ; clc
                ; adc enter_name_file_name_length ; + length( edited item text )
                tax
                inx
                stx enter_name_enter_name_x

                ; initial cursor Y = border top row Y + 1
                ldy #(EDIT_FIELD_DRAW_BORDER_ROW + 1)
                sty enter_name_enter_name_y

                ; initial cursor screen = border screen + initial cursor X + initial cursor Y * 40
                lda #>(EDIT_FIELD_DRAW_BORDER_SCREEN + 1 * 40)
                sta enter_name_enter_name_scr_hi
                lda #<(EDIT_FIELD_DRAW_BORDER_SCREEN + 1 * 40)
                clc
                adc enter_name_enter_name_x
                sta enter_name_enter_name_scr_lo
                bcc *+5
                inc enter_name_enter_name_scr_hi

                ; initial cursor colour = border colour + initial cursor X + initial cursor Y * 40
                lda enter_name_enter_name_scr_lo
                sta enter_name_enter_name_col_lo
                lda enter_name_enter_name_scr_hi
                clc
                adc #>($d800 - IDE64_LIST_SCREEN)
                sta enter_name_enter_name_col_hi

                rts
;----------------------------------
enter_name_draw_edit_field

                lda $d021
                jsr enter_name_set_enter_name_colours

                ldx #<EDIT_FIELD_DRAW_BORDER_SCREEN
                ldy #>EDIT_FIELD_DRAW_BORDER_SCREEN
                stx $fb
                sty $fc

                jsr enter_name_get_draw_border_offset_x
                jsr dir_navigator_add_A_to_fb_fc

                ldy #$00
                lda #$70 ; top-left corner
                sta ($fb),y

                jsr enter_name_draw_horizontal_line

                ldy enter_name_text_width
                iny
                lda #$6e ; top-right corner
                sta ($fb),y

                ldy #$28
                lda #$5d ; vertical line
                sta ($fb),y
                tya
                sec
                adc enter_name_text_width
                tay
                lda #$5d ; vertical line
                sta ($fb),y

                lda #$50
                jsr dir_navigator_add_A_to_fb_fc

                ldy #$00
                lda #$6d ; bottom-left corner
                sta ($fb),y

                jsr enter_name_draw_horizontal_line

                ldy enter_name_text_width
                iny
                lda #$7d ; bottom-right corner
                sta ($fb),y

                jsr enter_name_clear_user_input

                lda dir_navigator_frame_colour
                jsr enter_name_set_enter_name_colours

                rts
;----------------------------------
; $fb/$fc - screen vector where to begin drawing of an entire edit field row
; enter_name_text_width - length of line (number of characters to draw)
enter_name_draw_horizontal_line

                ldx #$00
                ldy #$01
                lda #$40 ; top border line
                sta ($fb),y
                iny
                inx
                cpx enter_name_text_width
                bne *-7
                rts
;----------------------------------
; .A - text colour to set
enter_name_set_enter_name_colours

                ldx #<EDIT_FIELD_DRAW_BORDER_COLORS
                ldy #>EDIT_FIELD_DRAW_BORDER_COLORS
                stx $fb
                sty $fc

                sta *+3+6+2+1

                jsr enter_name_get_draw_border_offset_x
                jsr dir_navigator_add_A_to_fb_fc

                ldx #$02

                lda #$ff
                ldy #$00
                sta ($fb),y
                iny
                cpy enter_name_full_width
                bne *-6

                lda #$28
                jsr dir_navigator_add_A_to_fb_fc

                dex
                bpl *-12-5-1

                rts
;----------------------------------
; Compute draw border offset and put it to A:
enter_name_get_draw_border_offset_x

                lda #$28
                sec
                sbc enter_name_full_width
                lsr
                rts
;----------------------------------
#macro enter_name_disable_cursor_blink()
{
                lda #$01
                sta $cc
}
;----------------------------------
#macro enter_name_enable_cursor_blink()
{
                lda #$00
                sta $cc
}
;----------------------------------
#macro enter_name_control_character_handler(char,handler)
{
                lda enter_name_key_pressed
                cmp #{char}
                bne *+5
                jmp {handler}
}
;----------------------------------
#macro enter_name_special_character_handler(char)
{
                lda enter_name_key_pressed
                cmp #{char}
                bne *+5
                jmp enter_name_key_handler
}
;----------------------------------
#macro enter_name_character_range_handler(from,to)
{
                ldx #{from}
                ldy #{to}
                jsr enter_name_accept_character_range
                bcs *+5
                jmp enter_name_key_handler
}
;----------------------------------
enter_name_get_user_input

                ; Set cursor position:
                lda enter_name_enter_name_x
                clc
                adc enter_name_file_name_length
                tax
                ldy enter_name_enter_name_y
                stx $d3   ; cursor column on current line
                sty $d6   ; current screen line number of cursor
                jsr $e56c ; set screen pointers

                .enter_name_enable_cursor_blink()

                jsr enter_name_write_file_name

                lda #$00
                sta $c6
;----------------------------------
enter_name_wait_for_key

                lda dir_navigator_external_keyboard_scan
                bne *+9
                lda #$00
                sta $c6
                jsr $ff9f
                jsr $ffe4

                sta enter_name_key_pressed

                ; Accept ESCAPE key ($5f):
                .enter_name_control_character_handler($5f,enter_name_key_cancelled)

                ; Accept RETURN key ($0d):
                .enter_name_control_character_handler($0d,enter_name_key_confirmed)

                ; Accept INST/DEL key ($14):
                .enter_name_control_character_handler($14,enter_name_key_deleted)

                ; Accept all characters between $30 and $39 (inclusive):
                .enter_name_character_range_handler($30,$39)

                ; Accept all characters between $41 and $5a (inclusive):
                .enter_name_character_range_handler($41,$5a)

                ; Accept all characters between $61 and $7a (inclusive):
                .enter_name_character_range_handler($61,$7a)

                ; Accept all characters between $c1 and $da (inclusive):
                .enter_name_character_range_handler($c1,$da)

                ; Accept special character $20 (" "):
                .enter_name_special_character_handler($20)

                ; Accept special character $21 ("!"):
                .enter_name_special_character_handler($21)

                ; Accept special character $2d ("-"):
                .enter_name_special_character_handler($2d)

                ; Accept special character $2e ("."):
                .enter_name_special_character_handler($2e)

                ; Accept special character $a4 ("_"):
                .enter_name_special_character_handler($a4)

                jmp enter_name_wait_for_key
;----------------------------------
enter_name_key_deleted

                ; Check if file name length has still something to delete:
                lda enter_name_file_name_length
                bne *+5
                jmp enter_name_wait_for_key

                ; Decrement file name length after deleting an item:
                dec enter_name_file_name_length

                ; Display updated text:
                jsr enter_name_write_file_name

                ; Move cursour offset to a new (previous) position:
                dec $d3   ; cursor column on current line
                jsr $e56c ; set screen pointers

                jmp enter_name_wait_for_key
;----------------------------------
enter_name_key_handler

                ; Check if file name length has not yet reached maximum:
                lda enter_name_file_name_length
                cmp #$10
                bne *+5
                jmp enter_name_wait_for_key

                ; Insert a new character at the end of input user text:
                lda enter_name_key_pressed
                jsr petscii_to_screen_code
                ldx enter_name_file_name_length
                sta enter_name_file_name_text,x

                ; Increment file name length after inserting a new char:
                inc enter_name_file_name_length

                ; Display updated text:
                jsr enter_name_write_file_name

                ; Move cursour offset to a new (next) position:
                inc $d3   ; cursor column on current line
                jsr $e56c ; set screen pointers

                jmp enter_name_wait_for_key
;----------------------------------
enter_name_key_cancelled

                .enter_name_disable_cursor_blink()

                jsr enter_name_clear_user_input

                sec
                rts
;----------------------------------
enter_name_key_confirmed

                ; Check if user has entered anything:
                lda enter_name_file_name_length
                bne *+5
                jmp enter_name_wait_for_key

                .enter_name_disable_cursor_blink()

                jsr enter_name_clear_user_input

                clc
                rts
;----------------------------------
enter_name_clear_user_input

                lda enter_name_enter_name_scr_lo
                sta $fb
                lda enter_name_enter_name_scr_hi
                sta $fc

                ldy #$00 ; file name offset
                cpy enter_name_text_width
                beq *+10
                lda #$20
                sta ($fb),y
                iny
                jmp *-10

                rts
;----------------------------------
set_screen_and_colour_vectors_to_input_field

                lda enter_name_enter_name_scr_lo
                sta $fb
                lda enter_name_enter_name_scr_hi
                sta $fc

                lda enter_name_enter_name_col_lo
                sta $fd
                lda enter_name_enter_name_col_hi
                sta $fe

                rts
;----------------------------------
enter_name_write_file_name

                jsr set_screen_and_colour_vectors_to_input_field

                ldy #$00 ; file name offset
                cpy enter_name_file_name_length
                beq *+16
                lda enter_name_file_name_text,y
                sta ($fb),y
                lda dir_navigator_regular_colour
                sta ($fd),y
                iny
                jmp *-16

                lda enter_name_file_name_length
                jsr dir_navigator_add_A_to_fb_fc

                lda enter_name_file_name_length
                jsr dir_navigator_add_A_to_fd_fe

                ldy #$00 ; file name static end text offset
enter_name_auto_suffix = * + 1
                lda enter_name_auto_suffix,y
                sta ($fb),y
                lda dir_navigator_frame_colour
                sta ($fd),y
                iny
                cpy enter_name_auto_suffix_length
                bne *-14

                ; IF current text length < maximum text length
                ; THEN write an empty space at the end (need for DELETE)
                ldx enter_name_file_name_length
                cpx enter_name_max_length
                beq *+6

                lda #$20
                sta ($fb),y

                rts
;----------------------------------
; Accept all characters between .X and .Y (inclusive)
; INPUT:
; .X - bottom range byte value
; .Y - top range byte value
; OUTPUT:
; .C = 0 - character accepted
; .C = 1 - character *NOT* accepted
;----------------------------------
enter_name_accept_character_range

                stx *+12
                iny
                sty *+18

                lda enter_name_key_pressed
                sec
                sbc #$ff
                bcs *+4
                sec
                rts

                lda enter_name_key_pressed
                sec
                sbc #$ff
                rts
;----------------------------------
enter_name_auto_suffix_length .db $00
enter_name_min_length         .db $00
enter_name_max_length         .db $00
;----------------------------------
enter_name_text_width         .db $00
enter_name_full_width         .db $00
enter_name_enter_name_x       .db $00
enter_name_enter_name_y       .db $00
enter_name_enter_name_scr_lo  .db $00
enter_name_enter_name_scr_hi  .db $00
enter_name_enter_name_col_lo  .db $00
enter_name_enter_name_col_hi  .db $00
;----------------------------------
enter_name_file_name_length   .db $00
enter_name_file_name_text     .dsb $10,$00 ; must be >= enter_name_max_length
;----------------------------------
enter_name_key_pressed        .db $00
;----------------------------------
enter_name_rename_dir_instructions

                ldx #<enter_name_rename_instructions
                ldy #>enter_name_rename_instructions
                jsr dir_navigator_write_text

                jmp enter_name_edit_field_instructions
;----------------------------------
enter_name_create_dir_instructions

                ldx #<enter_name_create_instructions
                ldy #>enter_name_create_instructions
                jsr dir_navigator_write_text

                jmp enter_name_edit_field_instructions
;----------------------------------
enter_name_edit_field_instructions

                ldx #<enter_name_edit_instructions
                ldy #>enter_name_edit_instructions
                jsr dir_navigator_write_text

                jsr dir_navigator_write_text_line
                jmp dir_navigator_write_text_line
;----------------------------------
enter_name_rename_instructions

                .db 1,23
                .dp "RETURN - RENAME DIRECTORY  "
                .db $5f
                .dp " - CANCEL"
                .db $00
;----------------------------------
enter_name_create_instructions

                .db 1,23
                .dp "RETURN - CREATE DIRECTORY  "
                .db $5f
                .dp " - CANCEL"
                .db $00
;----------------------------------
enter_name_edit_instructions

                .db 1,17
                .dp "INSTRUCTIONS:"
                .db $00
                .db 1,19
                .dp "ALPHANUMERIC CHARACTERS - TYPE NAME"
                .db $00
                .db 1,21
                .dp "INST/DEL - DELETE THE LAST CHARACTER"
                .db $00
;----------------------------------
enter_name_remove_dir_instructions

                ldx #<enter_name_remove_instructions
                ldy #>enter_name_remove_instructions
                jsr dir_navigator_write_text

                jsr dir_navigator_write_text_line
                jsr dir_navigator_write_text_line
                jmp dir_navigator_write_text_line
;----------------------------------
enter_name_remove_instructions

                .db 1,17
                .dp "INSTRUCTIONS:"
                .db $00
                .db 1,19
                .dp "Y - CONFIRM DIRECTORY REMOVAL"
                .db $00
                .db 1,21
                .dp "N - CANCEL DIRECTORY DELETION"
                .db $00
                .db 1,23
                .db $5f
                .dp " - RETURN BACK TO DIR NAVIGATOR"
                .db $00
;----------------------------------
enter_name_write_are_you_sure_y_n

                jsr set_screen_and_colour_vectors_to_input_field

                ldy #$00
                lda enter_name_are_you_sure_y_n,y
                sta ($fb),y
                lda dir_navigator_regular_colour
                sta ($fd),y
                iny
                cpy #ENTER_NAME_ARE_YOU_SURE_Y_N_LENGTH
                bne *-13

                rts
;----------------------------------
enter_name_are_you_sure_y_n .ds "Are you sure (Y/N)?  "
ENTER_NAME_ARE_YOU_SURE_Y_N_LENGTH = * - enter_name_are_you_sure_y_n
;----------------------------------
enter_name_get_user_confirmation

                lda dir_navigator_regular_colour
                sta $0286

                jsr enter_name_write_are_you_sure_y_n

                ; Set cursor position:
                lda enter_name_enter_name_x
                clc
                adc #(ENTER_NAME_ARE_YOU_SURE_Y_N_LENGTH - 1)
                tax
                ldy enter_name_enter_name_y
                stx $d3   ; cursor column on current line
                sty $d6   ; current screen line number of cursor
                jsr $e56c ; set screen pointers

                ; Setting this colour doesn't seem to have any effect, why? :(
                ; lda dir_navigator_highlight_colour
                ; sta $0286

                .enter_name_enable_cursor_blink()

                jsr enter_name_wait_for_Y_N_key_selection

                lda dir_navigator_frame_colour
                sta $0286

                rts
;----------------------------------
; RESULT: .C = 0 -> "N", .C = 1 -> "Y"
;----------------------------------
enter_name_wait_for_Y_N_key_selection

enter_name_wait_for_Y_N

                lda #$00
                sta $c6
                jsr $ff9f
                jsr $ffe4

                cmp #KEY_N
                beq enter_name_wait_select_N

                cmp #KEY_n
                beq enter_name_wait_select_N

                cmp #KEY_Y
                beq enter_name_wait_select_Y

                cmp #KEY_y
                beq enter_name_wait_select_Y

                cmp #ESCAPE
                beq enter_name_wait_select_N

                jmp enter_name_wait_for_Y_N

enter_name_wait_select_N

                .enter_name_disable_cursor_blink() ; 4 bytes, destroys .A

                lda #KEY_N
                jsr $ffd2

                ; jsr wait_until_N_key_is_released

                clc
                rts

enter_name_wait_select_Y

                .enter_name_disable_cursor_blink() ; 4 bytes, destroys .A

                lda #KEY_Y
                jsr $ffd2

                ; jsr wait_until_Y_key_is_released

                sec
                rts
;----------------------------------