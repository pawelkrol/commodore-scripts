CHANGES
=======

Revision history for `directory-navigator`, a hierarchical lists browsing library for Commodore 64.

0.05-SNAPSHOT (2023-02-09)
--------------------------

* Replace bottom-up merge sort with quick sort (measured to be faster by 5-10%)
* Fix bug with populating directory navigator with a single element list (when a single element has not been shown at all)
* Enable an optional selection of the current directory after pressing the space bar on the `..,DIR` item

0.04 (2021-01-08)
-----------------

* Provide an option to enable external keyboard scan (skipping calls to "jsr $ff9f" when enabled). This might be useful when you want to turn key repeat on, or have a default IRQ routine enabled (which does keyboard scanning already).
* Add comprehensive unit-tests for `sort_dir_entries` subroutine
* Fix directory browser's file name comparison subroutine:
    * Replace byte comparison based on screen codes with PETSCII
    * Order digits in front of letters
    * Recognise the end of file names
* Replace selection sort with bottom-up merge sort (measured to be faster by 60%)

0.03 (2015-06-24)
-----------------

* Speed up directory listing by improving sorting algorithm (sort item vectors instead of full directory entries).

0.02 (2015-06-17)
-----------------

* Add support for an optional creation/deletion/renaming of disk directories while navigating IDE64 directory tree.

0.01 (2015-04-03)
-----------------

* Initial version (supports full-fledged navigation over hierarchical lists, provides example of browsing IDE64 directory tree and file selection based on predefined criteria).
