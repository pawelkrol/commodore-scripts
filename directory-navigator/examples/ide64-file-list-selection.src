;----------------------------------
file_list_selection_setup

                lda #<file_list_selection_headline
                sta dir_navigator_headline_text+0
                lda #>file_list_selection_headline
                sta dir_navigator_headline_text+1

                lda #<read_dir
                sta dir_navigator_populate_list+0
                lda #>read_dir
                sta dir_navigator_populate_list+1

                jsr init_current_filename_item

                ; In the context of a file selection pressing a space key
                ; is always ignored, so this callback function will never
                ; get reached from within list selection component:
                ; sta dir_navigator_process_space_key_press+0
                ; sta dir_navigator_process_space_key_press+1

                lda #<process_selected_plugin_executable
                sta dir_navigator_process_return_key_press+0
                lda #>process_selected_plugin_executable
                sta dir_navigator_process_return_key_press+1

                lda #<cancel_plugin_filename_selection
                sta dir_navigator_process_escape_key_press+0
                lda #>cancel_plugin_filename_selection
                sta dir_navigator_process_escape_key_press+1

                lda #<file_list_selection_instructions
                sta dir_navigator_instructions+0
                lda #>file_list_selection_instructions
                sta dir_navigator_instructions+1
                lda #$03
                sta dir_navigator_instructions+2

                ; No customised instructions text colours, use default colours:
                lda #$00
                sta dir_navigator_instructions_set_colours+1

                lda #$0e
                sta dir_navigator_frame_colour
                lda #$0f
                sta dir_navigator_regular_colour
                lda #$07
                sta dir_navigator_highlight_colour

                lda #<DIR_LIST_ITEMS
                sta dir_navigator_items+0
                lda #>DIR_LIST_ITEMS
                sta dir_navigator_items+1

                lda #<never_ignore_return_key_press
                sta dir_navigator_ignore_return_selection+0
                lda #>never_ignore_return_key_press
                sta dir_navigator_ignore_return_selection+1

                lda #<always_ignore_space_key_press
                sta dir_navigator_ignore_f6_f8_selection+0
                lda #>always_ignore_space_key_press
                sta dir_navigator_ignore_f6_f8_selection+1

                lda #<always_ignore_space_key_press
                sta dir_navigator_ignore_space_selection+0
                lda #>always_ignore_space_key_press
                sta dir_navigator_ignore_space_selection+1

                lda #<set_come_back_current_item
                sta dir_navigator_set_come_back_vector+0
                lda #>set_come_back_current_item
                sta dir_navigator_set_come_back_vector+1

                lda #<no_selection_from_empty_list_possible
                sta dir_navigator_empty_list_error_handler+0
                lda #>no_selection_from_empty_list_possible
                sta dir_navigator_empty_list_error_handler+1

                lda #$00
                sta dir_navigator_allow_quit_selection

                jsr change_working_partition
                jsr change_plugin_directory

                ; Setup local callbacks:
                lda #<file_list_selection_skip_dir_new_entry
                sta skip_dir_new_entry+0
                lda #>file_list_selection_skip_dir_new_entry
                sta skip_dir_new_entry+1

                ; In the context of a file selection there is no need for an
                ; additional input from user, hence F6/F7/F8 keys are inactive
                ; (only needed when compiling "WITH_DISK_OPERATIONS" parameter):
                ; sta dir_navigator_process_f6_key_argument+0
                ; sta dir_navigator_process_f6_key_argument+1
                ; sta dir_navigator_process_f7_key_argument+0
                ; sta dir_navigator_process_f7_key_argument+1
                ; sta dir_navigator_process_f8_key_argument+0
                ; sta dir_navigator_process_f8_key_argument+1

                rts
;----------------------------------
; Callback to test if currently processed directory item should be included in
; the final list displayed to a user:
;  .C = 1 - include this entry
;  .C = 0 - skip this entry
;----------------------------------
file_list_selection_skip_dir_new_entry

                ; IF current item's type != "prg", THEN hide it from a user:
                ldx DIR+0
                ldy DIR+1
                jsr test_if_item_extension_is_prg
                bcc *+4
                clc
                rts
                sec
                rts
;----------------------------------
change_plugin_directory

                ldx #$00

                ; Move source pointer +1 position to skip partition number:
                lda SELECTED_DIR + 1,x
                cmp #$3a  ; ":"
                beq *+9
                sta change_directory_filename,x
                inx
                jmp *-11

                ; Replace trailing ":" in the path with a null-terminator:
                lda #$00
                sta change_directory_filename,x

                jmp enter_directory
;----------------------------------
; Always ignore SPACE selection:
; .C=1 - ignore selection
;----------------------------------
always_ignore_space_key_press

                sec
                rts
;----------------------------------
; Never ignore RETURN selection:
; .C=0 - proceed with RETURN
;----------------------------------
never_ignore_return_key_press

                clc
                rts
;----------------------------------
; Return value of this callback indicates whether list selection came to an end:
;  .C=1 - selection over, return from "dir_navigator_selection" subroutine
;  .C=0 - selection continues, jump back to "dir_navigator_reload" and wait for key
;----------------------------------
process_selected_plugin_executable

                jsr set_fb_fc_vector_to_current_item_name
                ldx #$00
                ldy #$01
                jsr append_selection_to_current_path

                jsr convert_selected_file_to_petscii

                sec
                rts
;----------------------------------
; This subroutine will be executed if the list of PRG files to choose from is
; empty upon directory listing initialisation (no dedicated error handling is
; required in this particular context though):
no_selection_from_empty_list_possible

                rts
;----------------------------------
init_current_filename_item

                lda #$00
                sta current_path_length
                sta file_list_selection_cancelled

                rts
;----------------------------------
; Return value of this callback indicates whether list selection came to an end:
;  .C=1 - selection over, return from "dir_navigator_selection" subroutine
;  .C=0 - selection continues, jump back to "dir_navigator_reload" and wait for key
;----------------------------------
cancel_plugin_filename_selection

                ; Indicate cancellation of a plugin filename selection to
                ; a caller of the "dir_navigator_selection" subroutine:
                lda #$01
                sta file_list_selection_cancelled

                sec
                rts
;----------------------------------
file_list_selection_cancelled .db 0
;----------------------------------
file_list_selection_headline

                .db 1,1
                .dp "SELECT PLUGIN EXECUTABLE:"
                .db $00
;----------------------------------
file_list_selection_instructions

                .db 1,19
                .dp "INSTRUCTIONS:"
                .db $00
                .db 1,21
                .dp "CURSOR KEYS - NAVIGATE   "
                .db $5f ; left arrow
                .dp " - CANCEL"
                .db $00
                .db 1,23
                .dp "RETURN - SELECT HIGHLIGHTED PROGRAM"
                .db $00
;----------------------------------
