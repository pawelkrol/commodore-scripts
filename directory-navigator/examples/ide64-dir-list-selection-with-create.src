;----------------------------------
FRAME_COLOUR      = $0e
REGULAR_COLOUR    = $0f
HIGHLIGHT_COLOUR  = $07
INSTR_HIGH_COLOUR = $03 ; REGULAR_COLOUR
INSTR_LOW_COLOUR  = FRAME_COLOUR
;----------------------------------
dir_list_selection_setup

                lda #<dir_list_selection_headline
                sta dir_navigator_headline_text+0
                lda #>dir_list_selection_headline
                sta dir_navigator_headline_text+1

                lda #<read_dir
                sta dir_navigator_populate_list+0
                lda #>read_dir
                sta dir_navigator_populate_list+1

                ; Setting final selection is equivalent to entering selected
                ; subdirectory for browsing:
                lda #<set_final_directory_selection_item
                sta dir_navigator_process_space_key_press+0
                lda #>set_final_directory_selection_item
                sta dir_navigator_process_space_key_press+1

                lda #<change_directory_and_update_current_item
                sta dir_navigator_process_return_key_press+0
                lda #>change_directory_and_update_current_item
                sta dir_navigator_process_return_key_press+1

                lda #<cancel_directory_selection
                sta dir_navigator_process_escape_key_press+0
                lda #>cancel_directory_selection
                sta dir_navigator_process_escape_key_press+1

                lda #<dir_list_selection_instructions
                sta dir_navigator_instructions+0
                lda #>dir_list_selection_instructions
                sta dir_navigator_instructions+1
                lda #$06
                sta dir_navigator_instructions+2

                ; Setup callback to apply custom instructions text colours:
                lda #<set_main_instructions_text_colours
                sta dir_navigator_instructions_set_colours+0
                lda #>set_main_instructions_text_colours
                sta dir_navigator_instructions_set_colours+1

                lda #FRAME_COLOUR
                sta dir_navigator_frame_colour
                lda #REGULAR_COLOUR
                sta dir_navigator_regular_colour
                lda #HIGHLIGHT_COLOUR
                sta dir_navigator_highlight_colour

                lda #<DIR_LIST_ITEMS
                sta dir_navigator_items+0
                lda #>DIR_LIST_ITEMS
                sta dir_navigator_items+1

                lda #<ignore_return_selection
                sta dir_navigator_ignore_return_selection+0
                lda #>ignore_return_selection
                sta dir_navigator_ignore_return_selection+1

                lda #<ignore_f6_f8_selection
                sta dir_navigator_ignore_f6_f8_selection+0
                lda #>ignore_f6_f8_selection
                sta dir_navigator_ignore_f6_f8_selection+1

                lda #<ignore_space_selection
                sta dir_navigator_ignore_space_selection+0
                lda #>ignore_space_selection
                sta dir_navigator_ignore_space_selection+1

                lda #<set_come_back_current_item
                sta dir_navigator_set_come_back_vector+0
                lda #>set_come_back_current_item
                sta dir_navigator_set_come_back_vector+1

                ; In the context of a directory selection empty list error
                ; will never occur, because we push at least parent directory
                ; link ".." to the list every time before rendering the list:
                ; sta dir_navigator_empty_list_error_handler+0
                ; sta dir_navigator_empty_list_error_handler+1

                lda #$00
                sta dir_navigator_allow_quit_selection

                ; Setup local callbacks:
                lda #<dir_list_selection_skip_dir_new_entry
                sta skip_dir_new_entry+0
                lda #>dir_list_selection_skip_dir_new_entry
                sta skip_dir_new_entry+1

                ; Initialise input field used to enter directory name to create
                ; or rename, and to display a confirmation prompt for deletion:
                jsr initialize_user_input_edit_field

                ; Setup a callback for processing a currently highlighted file
                ; after user updates its name in a browser after pressing F6 key
                ; (only needed when compiling "WITH_DISK_OPERATIONS" parameter):
                lda #<rename_dir
                sta dir_navigator_process_f6_key_argument+0
                lda #>rename_dir
                sta dir_navigator_process_f6_key_argument+1

                ; Setup a callback for processing an argument entered into an
                ; input field which appears in a browser after pressing F7 key
                ; (only needed when compiling "WITH_DISK_OPERATIONS" parameter):
                lda #<create_dir
                sta dir_navigator_process_f7_key_argument+0
                lda #>create_dir
                sta dir_navigator_process_f7_key_argument+1

                ; Setup a callback for processing a currently highlighted file
                ; after user confirms an action associated with an F8 key by
                ; pressing "Y" after being prompted to answer "are you sure?"
                ; question which pops up in a tool right after pressing F8 key
                ; (only needed when compiling "WITH_DISK_OPERATIONS" parameter):
                lda #<remove_dir
                sta dir_navigator_process_f8_key_argument+0
                lda #>remove_dir
                sta dir_navigator_process_f8_key_argument+1

                rts
;----------------------------------
; Setup the following input parameters for a new directory name input field:
;  * auto_suffix = ",dir"
;  * auto_suffix_length = 4
;  * min_length = 1
;  * max_length = 16
;----------------------------------
initialize_user_input_edit_field

                ; auto_suffix = ",dir"
                lda #<enter_field_auto_suffix
                sta enter_name_auto_suffix+0
                lda #>enter_field_auto_suffix
                sta enter_name_auto_suffix+1

                ; auto_suffix_length = 4
                lda #ENTER_FIELD_AUTO_SUFFIX_LENGTH
                sta enter_name_auto_suffix_length

                ; min_length = 1
                lda #$01
                sta enter_name_min_length

                ; max_length = 16
                lda #$10
                sta enter_name_max_length

                rts
;----------------------------------
enter_field_auto_suffix .ds ",dir"
ENTER_FIELD_AUTO_SUFFIX_LENGTH = * - enter_field_auto_suffix
;----------------------------------
; Callback to test if currently processed directory item should be included in
; the final list displayed to a user:
;  .C = 1 - include this entry
;  .C = 0 - skip this entry
;----------------------------------
dir_list_selection_skip_dir_new_entry

                ; Always filter out files without any defined type/extension:
                ldx DIR+0
                ldy DIR+1
                jsr test_if_item_extension_is_empty

                rts
;----------------------------------
; Return value of this callback indicates whether list selection came to an end:
;  .C=1 - selection over, return from "dir_navigator_selection" subroutine
;  .C=0 - selection continues, jump back to "dir_navigator_reload" and wait for key
;----------------------------------
set_final_directory_selection_item

#ifdef ENABLE_DOTDOT_SELECTION

                ; IF current item's name == ".."
                jsr is_current_item_name_dotdot
                bcc *+5
                ; THEN do not change current directory upon item's selection

#endif ; ENABLE_DOTDOT_SELECTION

                jsr change_current_directory

                sec
                rts
;----------------------------------
; Ignore F6/F8 selection if:
;  * Current value is not "*,DIR"
;  * Current value is "..,DIR"
; .C=1 - ignore selection
; .C=0 - proceed with SPACE
;----------------------------------
ignore_f6_f8_selection

                ; IF current item's type != "DIR"
                jsr is_current_item_type_dir
                bcc *+3
                rts

                ; OR current item's name == ".."
                jsr is_current_item_name_dotdot
                bcc *+4
                clc
                rts

                ; THEN ignore selection and do not react on SPACE key press
                sec
                rts
;----------------------------------
; ENABLE_DOTDOT_SELECTION = 0
;
; Ignore SPACE selection if:
;  * Current value is not "*,DIR"
;  * Current value is "..,DIR"
; .C=1 - ignore selection
; .C=0 - proceed with SPACE
;----------------------------------
; ENABLE_DOTDOT_SELECTION = 1
;
; Ignore SPACE selection if:
;  * Current value is not "*,DIR"
; .C=1 - ignore selection
; .C=0 - proceed with SPACE
;----------------------------------
ignore_space_selection

#ifndef ENABLE_DOTDOT_SELECTION

                jmp ignore_f6_f8_selection

#else ; ENABLE_DOTDOT_SELECTION

                ; IF current item's type != "DIR"
                ; THEN ignore selection and do not react on SPACE key press
                jmp is_current_item_type_dir

#endif ; ENABLE_DOTDOT_SELECTION
;----------------------------------
; Ignore RETURN selection if:
;  * Current value is not "*,DIR"
; .C=1 - ignore selection
; .C=0 - proceed with RETURN
;----------------------------------
ignore_return_selection

                ; IF current item's type != "DIR"
                ; THEN ignore selection and do not react on RETURN key press
                jmp is_current_item_type_dir
;----------------------------------
; Return value of this callback indicates whether list selection came to an end:
;  .C=1 - selection over, return from "dir_navigator_selection" subroutine
;  .C=0 - selection continues, jump back to "dir_navigator_reload" and wait for key
;----------------------------------
change_directory_and_update_current_item

                jsr dir_navigator_display_loading_text

                jsr change_current_directory

                clc
                rts
;----------------------------------
; Return value of this callback indicates whether list selection came to an end:
;  .C=1 - selection over, return from "dir_navigator_selection" subroutine
;  .C=0 - selection continues, jump back to "dir_navigator_reload" and wait for key
;----------------------------------
cancel_directory_selection

                ; It is always possible to cancel directory selection, because
                ; we will simply use current working directory as a destination
                ; directory for writing created D64 disk image into:
                sec
                rts
;----------------------------------
dir_list_selection_headline

                .db 1,1
                .dp "SELECT DESTINATION DIRECTORY:"
                .db $00
;----------------------------------
set_main_instructions_text_colours

                lda #<($d800 + ($28 * 17))
                sta $fb
                lda #>($d800 + ($28 * 17))
                sta $fc

                ldx #$00
llop            lda dir_list_selection_instructions_colour_scheme,x
                cmp #$ff
                bne *+3
                rts

                sta coll+1
                inx
                lda dir_list_selection_instructions_colour_scheme,x
                sta numm+1
                inx

                ldy #$00
coll            lda #$ff
                sta ($fb),y
                iny
numm            cpy #$ff
                bne *-5

                tya
                jsr dir_navigator_add_A_to_fb_fc

                jmp llop
;----------------------------------
dir_list_selection_instructions_colour_scheme

                .db INSTR_LOW_COLOUR,$50
                .db INSTR_HIGH_COLOUR,$1a
                .db INSTR_LOW_COLOUR,$0e
                .db INSTR_HIGH_COLOUR,$07
                .db INSTR_LOW_COLOUR,$21
                .db INSTR_HIGH_COLOUR,$06
                .db INSTR_LOW_COLOUR,$22
                .db INSTR_HIGH_COLOUR,$02
                .db INSTR_LOW_COLOUR,$26
                .db INSTR_HIGH_COLOUR,$03
                .db INSTR_LOW_COLOUR,$0b
                .db INSTR_HIGH_COLOUR,$02
                .db INSTR_LOW_COLOUR,$0b
                .db INSTR_HIGH_COLOUR,$02
                .db INSTR_LOW_COLOUR,$09
                .db $ff
;----------------------------------
dir_list_selection_instructions

                .db 1,17
                .dp "INSTRUCTIONS:"
                .db $00
                .db 1,19
                .dp "CURSOR LEFT/RIGHT/UP/DOWN - NAVIGATE"
                .db $00
                .db 1,20
                .dp "RETURN - ENTER HIGHLIGHTED DIRECTORY"
                .db $00
                .db 1,21
                .dp "SPACE - SELECT HIGHLIGHTED DIRECTORY"
                .db $00
                .db 1,22
                .db $5f ; left arrow
                .dp " - SELECT CURRENT DIRECTORY"
                .db $00
                .db 1,23
                .dp "F6 - RENAME  F7 - CREATE  F8 - DELETE"
                .db $00
;----------------------------------
