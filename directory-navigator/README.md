directory-navigator
===================

`directory-navigator` is a very generic component (no worries though, as it comes up bundled with very extensive examples of use) providing a ready-for-use functionality to browse over hierarchical lists, for example directory structure, as encountered in the [IDE64](http://ide64.org/) device. It is however not bound to [IDE64](http://ide64.org/) hardware in any specific way, apart from the functionality that a client himself may provide via customisable callback functions.

VERSION
-------

0.05-SNAPSHOT (2023-02-09)

INSTALLATION
------------

To build this application type the following:

    $ git clone git@bitbucket.org:pawelkrol/commodore-scripts.git
    $ cd commodore-scripts/directory-navigator
    $ export COMMODORE_SCRIPTS=..
    $ make

To execute the program type one of the following:

    $ make test
    $ make test-ide64

Optionally you can test a variant of a directory list selection browser with a built-in option to create new directories:

    $ make test WITH=DIR_CREATE
    $ make test-ide64 WITH=DIR_CREATE

COMPILE-TIME FEATURES
---------------------

| `dreamass` build flag     | Description                                                                                   |
| ------------------------- | --------------------------------------------------------------------------------------------- |
| `ENABLE_DOTDOT_SELECTION` | Enable selection of `..` by returning the current directory as a result (disabled by default) |

EXAMPLES
--------

The following ready-to-use `examples` have been implemented for an easier use of the `directory-navigator` module:

1. Directory list selection browser for [IDE64](http://ide64.org/): `ide64-dir-list-selection.src`.
2. Directory list selection browser for [IDE64](http://ide64.org/) with an option to create new directories while browsing directory structure: `ide64-dir-list-selection-with-create.src`.
3. File list selection browser for [IDE64](http://ide64.org/) `ide64-file-list-selection.src`.

They all operate using [IDE64](http://ide64.org/) device as their target directory tree. An original idea behind setting up these particular examples came from an implementation of a `man,usr` configuration file editor. Their goal is to enable easy selection of a specific directory from a disk, as well as a particular file from within that directory, using an intuitive graphical user interface. It is accomplished via a helper module named `ide64/directory-browser.src` that implements most of the callbacks expected by the `directory-navigator` module plus some additional [IDE64](http://ide64.org/)-specific routines. All the routines specific to directory versus file selection have been implemented in one of the dedicated source files: `ide64-dir-list-selection.src`, `ide64-dir-list-selection-with-create.src`, and `ide64-file-list-selection.src`. Everything is bound together by a very simple executable program named `plugin-selection.src`, which demonstrates how easily reusable all those components are. Apart from the specific calls to bring up selection windows, it also implements a few information screens displaying visual information about selections applied by a user as well as their consequences (for example selecting an empty directory as a source directory for plugin files renders an error, because no file name can be picked from an empty list).

Depending on your use-case, you may want to implement all callback functions yourself. You might also want to reuse some of the [IDE64](http://ide64.org/)-specific functionalities provided by a generic `ide64/directory-browser.src`. And finally you may as well simply pick one of the ready-to-use selectors, just by including the source code of one of the examples into your own program. Compile and run `plugin-selection.src` to see it all in action.

TODO
----

* Right-hand side vertical bar is not implemented at the moment. Instead of a static image it is meant to indicate view port size of a current directory listing as well as its current positioning within the list.

* When using an option to create new directories while browsing directory structure and navigating up the directory tree we would also like to end up highlighting an entry we have just left instead of a top element `".."` (it is implemented in the `set_come_back_current_item` subroutine, however it doesn't work, because we do not store information about the current path at all in this mode). IMPORTANT NOTE: When this is implemented, it needs to be ensured that upon program initialisation "cd/" is always performed (because user may start program from any directory, and "cd.." wouldn't have worked then, because current_path was not constructed yet).

* Set highlight on a directory that has been just created/renamed (shortly after make/rename directory command has been executed or on a directory reload? On reload sound better, because it could be using code that is already fulfilling an above bullet point).

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2015-2023 by Pawel Krol.

This software is distributed under the terms of the GNU LGPL license. See [LICENSE](https://bitbucket.org/pawelkrol/commodore-scripts/src/master/directory-navigator/LICENSE.md) for more information.
