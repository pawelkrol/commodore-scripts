import com.github.pawelkrol.CPU6502.ByteVal

import language.implicitConversions

case class DirEntry(
  size: Int,
  name: String,
  extension: FileType
) extends Ordered[DirEntry] {

  import PETSCII.ascii_to_petscii

  val numOfBlocks = ascii_to_petscii(size.toString)

  val fileName = ascii_to_petscii("\"%s\"".format(name))

  val fileType = ascii_to_petscii(extension())

  def compare(that: DirEntry) = {
    val extensionCompare = this.extension compareTo that.extension
    if (extensionCompare != 0)
      extensionCompare
    else
      this.name compareTo that.name
  }
}

object DirEntry {

  import PETSCII.petscii_to_ascii

  def apply(size: Int, name: String, extension: String): DirEntry = DirEntry(size, name, FileType(extension))

  def apply(bytes: Seq[ByteVal]): DirEntry = {

    val text = petscii_to_ascii(bytes)

    val size = text.drop(0x03).takeWhile(_ != 0x20).toInt
    val name = text.drop(0x0a).takeWhile(_ != 0x22)
    val extension = text.drop(0x1c).take(0x03)

    DirEntry(size, name, extension)
  }

  implicit def tuple2dirEntry(data: Tuple3[Int, String, String]): DirEntry = {
    val (size, name, extension) = data
    DirEntry(size, name, extension)
  }
}
