import com.github.pawelkrol.CPU6502.ByteVal

object PETSCII {

  private def to_ascii(byte: ByteVal) = {

    val code = byte & 0x7f
    if (byte != code)
      throw new RuntimeException("Invalid PETSCII code of converted text string: \"0x%02x\" (convertible codes include bytes between 0x00 and 0x7f)".format(byte))

    val ascii: ByteVal =
      if (code >= 0x61 && code <= 0x7a) // "A" .. "Z"
        code - 0x20
      else if (code >= 0x41 && code <= 0x5a) // "a" .. "z"
        code + 0x20
      else if (code == 0x7f)
        0x3f
      else
        code

    ascii.toChar
  }

  private def to_petscii(char: Char) = {

    val code = char & 0x7f
    if (char != code)
      throw new RuntimeException("Invalid ASCII code of converted text string: \"0x%02x\" (convertible codes include bytes between 0x00 and 0x7f)".format(char))

    val petscii =
      if (code >= 'A' && code <= 'Z')
        code + 32
      else if (code >= 'a' && code <= 'z')
        code - 32
      else
        code

    ByteVal(petscii)
  }

  def ascii_to_petscii(text: String): Seq[ByteVal] =
    text.split("").map(char => to_petscii(char.head))

  def petscii_to_ascii(bytes: Seq[ByteVal]): String =
    bytes.map(to_ascii(_)).mkString
}
