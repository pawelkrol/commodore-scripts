sealed trait FileType extends Ordered[FileType] {

  def apply(): String

  def compare(that: FileType) = this() compareTo that()
}

case class FileTypeDIR() extends FileType {

  def apply() = "dir"
}

case class FileTypePRG() extends FileType {

  def apply() = "prg"
}

case class FileTypeSEQ() extends FileType {

  def apply() = "seq"
}

case class FileTypeUSR() extends FileType {

  def apply() = "usr"
}

object FileType {

  def apply(extension: String) =
    extension match {
      case "dir" => FileTypeDIR()
      case "prg" => FileTypePRG()
      case "seq" => FileTypeSEQ()
      case "usr" => FileTypeUSR()
      case _ => throw new RuntimeException("Illegal file type: '%s'".format(extension))
    }
}
