import com.github.pawelkrol.CommTest.FunSpec

trait DirectoryNavigatorSpec extends FunSpec {

  private var _source: String = _

  protected def source: String = _source

  protected def source_=(name: String): Unit = {
    outputPrg = target(name, "prg")
    labelLog = target(name, "log")
  }

  private def target(name: String, extension: String) =
    "target/" + name + "-test." + extension

  protected def setup: Unit = {
    writeWordAt("DIR", labelLog("UNSORTED_DIR_LIST"))
    writeByteAt("dir_navigator_num_items", 0x00)
    // Setup local callbacks:
    writeWordAt("skip_dir_new_entry", labelLog("dir_list_selection_skip_dir_new_entry"))
    // Insert extra ".." entry for moving to the top directory:
    call("push_parent_directory_to_dir_list")
  }

  protected def add_dir_entry(file: DirEntry): Unit = {
    call("fill_current_entry_with_empty_spaces")
    writeBytesAt(readWordAt("DIR") + labelLog("ITEM_SIZE_OFFSET"), file.numOfBlocks: _*)
    writeBytesAt(readWordAt("DIR") + labelLog("ITEM_NAME_OFFSET"), file.fileName: _*)
    writeBytesAt(readWordAt("DIR") + labelLog("ITEM_TYPE_OFFSET"), file.fileType: _*)
    call("read_dir_add_new_entry")
  }

  protected def fetch_dir(from: String): Seq[DirEntry] = {
    val dirNavigatorNumItems = readByteAt("dir_navigator_num_items").toInt
    (0 until dirNavigatorNumItems).map(index =>
      DirEntry(readBytesAt(labelLog(from) + 34 * index, 34))
    )
  }

  protected def read_dir(directory: Seq[DirEntry]): Unit = {
    setup
    // Loop over added "directory" entries:
    directory.foreach { add_dir_entry(_) }
    call("initialise_sorting_list_vectors")
  }

  protected def rewrite_sorted_dir: Unit = {
    call("apply_sorting_list_vectors")
  }
}
