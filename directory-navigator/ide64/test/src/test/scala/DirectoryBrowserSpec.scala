import com.github.pawelkrol.CPU6502.ByteVal

class DirectoryBrowserSpec extends DirectoryNavigatorSpec {

  source = "directory-browser"

  private val testCases = Seq[Seq[DirEntry]](
    Seq(
      (0, "plugins", "dir"),
      (1, "man", "usr"),
      (21, "plugin-selection", "prg"),
      (0, "empty", "dir"),
    ),
    Seq(
      (5, "kla", "prg"),
      (5, "fcp", "prg"),
      (4, "mpic", "prg"),
      (4, "pic", "prg"),
      (4, "hpic", "prg")
    ),
    Seq(
      (24, "viewer", "prg"),
      (4, "mpic", "prg"),
      (5, "ani", "prg"),
      (25, "viewer vdc", "prg"),
      (5, "mp3", "prg"),
      (8, "d1m", "prg"),
      (7, "vt64beta", "prg"),
      (28, "vi65 80", "prg"),
      (6, "sca", "prg"),
      (9, "cpio", "prg"),
      (5, "fcp", "prg"),
      (0, "utils", "dir"),
      (6, "afli", "prg"),
      (6, "crt", "prg"),
      (9, "fun", "prg"),
      (4, "pic", "prg"),
      (19, "vd64beta", "prg"),
      (20, "vd81beta", "prg"),
      (8, "lbr", "prg"),
      (2, "tsm", "prg"),
      (28, "vi65", "prg"),
      (14, "shf", "prg"),
      (6, "fli", "prg"),
      (13, "d71", "prg"),
      (19, "vdfibeta", "prg"),
      (8, "d2m", "prg"),
      (40, "jpg", "prg"),
      (6, "z64", "prg"),
      (4, "lres", "prg"),
      (24, "zip", "prg"),
      (29, "vi65 64", "prg"),
      (0, "manusr", "dir"),
      (16, "d64l", "prg"),
      (6, "vd64", "prg"),
      (10, "d81", "prg"),
      (8, "shi", "prg"),
      (8, "unlnx", "prg"),
      (5, "kla", "prg"),
      (5, "ami", "prg"),
      (11, "sid", "prg"),
      (6, "vd81", "prg"),
      (8, "t64", "prg"),
      (0, "docs", "dir"),
      (5, "drp", "prg"),
      (8, "ifli", "prg"),
      (20, "vd71beta", "prg"),
      (8, "d4m", "prg"),
      (6, "vd71", "prg"),
      (2, "asm", "prg"),
      (14, "d64", "prg"),
      (8, "ppp", "prg"),
      (8, "tar", "prg"),
      (7, "ark", "prg"),
      (5, "drl", "prg"),
      (24, "vi65 40", "prg"),
      (4, "vt64", "prg"),
      (28, "vi65 53", "prg"),
      (24, "vi65 vdc", "prg")
    )
  ).zipWithIndex

  describe("sort_dir_entries") {
    context("mixed directory and file type entries") {
      for (tc <- testCases) {
        val (directory, index) = tc
        it("sorts directory entries, test case " + (index + 1)) {
          read_dir(directory)
          assert(fetch_dir("UNSORTED_DIR_LIST").drop(1) === directory)
          call
          rewrite_sorted_dir
          assert(fetch_dir("DIR_LIST_ITEMS").drop(1) === directory.sorted)
        }
      }
    }
  }
}
