data-compression
================

`data-compression` section provides several useful [Perl](http://www.perl.org/) scripts implementing data compression and decompression algorithms of miscellaneous Commodore archive formats.

zipcode-extractor.pl
--------------------

This script extracts a D64 disk image from a set of [ZipCode](http://ist.uwaterloo.ca/~schepers/formats/ZIP_DISK.TXT) archive files.

Example of usage:

    $ perl zipcode-extractor.pl 1\!Athanor

Given that all four [ZipCode](http://ist.uwaterloo.ca/~schepers/formats/ZIP_DISK.TXT) files are found, executing an above command will produce an `Athanor.d64` disk image in a current working directory.

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2015-2023 by Pawel Krol.

This library is free open source software; you can redistribute it and/or modify it under the same terms as Perl itself, either Perl version 5.8.6 or, at your option, any later version of Perl 5 you may have available.

PLEASE NOTE THAT IT COMES WITHOUT A WARRANTY OF ANY KIND!
