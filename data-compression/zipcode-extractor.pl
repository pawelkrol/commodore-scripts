#!/usr/bin/perl

=head1 NAME

zipcode-extractor.pl - Perl script to extract D64 disk images from ZipCode archives

=head1 SYNOPSIS

    perl zipcode-extractor.pl <SOURCE> [<TARGET>]

=cut

use bytes;
use strict;
use utf8;
use warnings;

use Data::Dumper;
use File::Slurp;
use Readonly;

Readonly our $OUT_SUFFIX => q{.d64};

my %names = get_options(@ARGV);
my $data = read_archive(@{$names{source}});
write_file($names{target}, { binmode => q{:raw} }, $data);

exit 0;

sub read_archive {
    my @files = @_;

    my @bin_data;

    my @skip_bytes = (4, 2, 2, 2);

    for my $file (@files) {
        my @bytes = map { ord } split //, read_file($file, binmode => q{:raw});
        push @bin_data, splice @bytes, shift @skip_bytes;
    }

    my $data;

    for (my $track = 1; $track <= 35; $track++) {
        my $max_sector = 17 + (($track < 31) ? 1 : 0) + (($track < 25) ? 1 : 0) + (($track < 18) ? 2 : 0);

        my @trackbuf_decoded;
        my @trackbuf;

        for (1 .. $max_sector) {
            my $track_ = shift @bin_data;
            my $sector = shift @bin_data;

            if (($track_ & 0x3f) != $track || $sector < 0 || $sector >= $max_sector || $trackbuf_decoded[$sector]) {
                show_error(qq{Source file is corrupted!});
            }

            $trackbuf_decoded[$sector] = 1;

            if ($track_ & 0x80) {
                my $count = 0;

                my $length = shift @bin_data;
                my $escape = shift @bin_data;

                while ($length--) {
                    my $byte = shift @bin_data;
                    if ($byte != $escape) {
                        push @{$trackbuf[$sector]}, $byte;
                        $count++;
                        if ($count > 256) {
                            show_error(qq{Source file is corrupted!});
                        }
                    }
                    elsif ($length >= 2) {
                        my $repnum = shift @bin_data;
                        my $byte = shift @bin_data;
                        if ($repnum < 0 || $repnum + $count > 256 || !defined $byte) {
                            show_error(qq{Source file is corrupted!});
                        }
                        push @{$trackbuf[$sector]}, ($byte) x $repnum;
                        $count += $repnum;
                        $length -= 2;
                    }
                    else {
                        show_error(qq{Source file is corrupted!});
                    }
                }

                if ($count != 256) {
                    show_error(qq{Source file is corrupted!});
                }
            }
            elsif ($track_ & 0x40) {
                my $byte = shift @bin_data;
                if (!defined $byte) {
                    show_error(qq{Source file is corrupted!});
                }
                $trackbuf[$sector]->[$_] = $byte for (0 .. 255);
            }
            else {
                my @bytes = splice @bin_data, 0, 256;
                if (grep { !defined } @bytes) {
                    show_error(qq{Source file is corrupted!});
                }
                $trackbuf[$sector] = \@bytes;
            }
        }

        for my $sector (0 .. $max_sector - 1) {
            $data .= join q{}, map { chr } @{$trackbuf[$sector]};
        }
    }

    if (@bin_data > 0) {
        show_error(qq{Source file is corrupted!});
    }

    return $data;
}

sub get_options {
    my ($source, $target) = @_;

    show_usage(qq{Source ZipCode archive not defined!}) unless defined $source;

    unless (defined $target) {
        ($target = $source) =~ s/^\d+[-!](.+)$/$1$OUT_SUFFIX/;
    }

    show_usage(qq{Target D64 disk image not defined!}) if $target eq $OUT_SUFFIX;

    show_usage(qq{Target D64 disk image "$target" already exists!}) if -e $target;

    my %names = (
      source => get_source_files($source),
      target => $target,
    );

    return %names;
}

sub get_source_files {
    my ($source) = @_;

    my $separator;

    if (-e $source) {
      if ($source =~ s/^\d+([-!])//) {
        $separator = $1;
      }
      else {
          show_usage(qq{Source ZipCode archive name unexpected!});
      }
    }
    else {
        show_usage(qq{Source ZipCode archive does not exist!});
    }

    my @names = map { qq{${_}${separator}${source}} } (1, 2, 3, 4);

    for my $name (@names) {
        show_usage(qq{Source ZipCode archive does not exist!}) unless -e $name;
    }

    return \@names;
}

sub show_usage {
    my ($message) = @_;

    print <<USAGE;

$message

Usage: $0 <SOURCE> [<TARGET>]

Options:

    <SOURCE> - file name of a source ZipCode archive (required)
    <TARGET> - file name of a target D64 disk image (optional)

This script extracts D64 disk images from ZipCode archives

(C) 2023-03-31 Paweł Król (DJ Gruby/Protovision/TRIAD)

USAGE

    exit 1;
}

sub show_error {
    my ($message) = @_;

    print <<USAGE;

$message

USAGE

    exit 2;
}
