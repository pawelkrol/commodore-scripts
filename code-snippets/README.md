Code Snippets
=============

`code-snippets` section provides several ready-to-use source code snippets that may become handy when resolving the most common assembly programming challenges. They can be easily included into any other source code file by using `include` directive of [dreamass](https://github.com/docbacardi/dreamass).

2-byte integer to screen codes conversion
-----------------------------------------

Including `itoa.src` source code file into your program exposes `itoa` subroutine for use. It converts a 2-byte integer value referred via `itoa_number` vector to a sequence of digits representing its decimal value in a human-readable form and writes them into a memory location identified via `itoa_string` vector (both vectors have to be properly initialized before actually calling a procedure).

This procedure requires 6 bytes of a zero page memory as defined at the top of a source code file in a `_itoa_zp` constant.

Example of use:

    ;------------------------
    #include "itoa.src"
    ;------------------------
    VALUE   = $4000
    ;------------------------
            lda #<VALUE
            sta number+0
            lda #>VALUE
            sta number+1

            lda #<number
            sta itoa_number+0
            lda #>number
            sta itoa_number+1

            lda #<decimal
            sta itoa_string+0
            lda #>decimal
            sta itoa_string+1

            jsr itoa
            ; decimal = "16384" which is screen code representation of a converted value
    ;------------------------
    number  .dsb 2,$00
    decimal .dsb 5,$00
    ;------------------------

As an extra bonus it is possible to remove leading zeroes from a converted number:

    ;------------------------
    #include "itoa.src"
    ;------------------------
    VALUE   = $1000
    ;------------------------
            lda #<VALUE
            sta number+0
            lda #>VALUE
            sta number+1

            lda #<number
            sta itoa_number+0
            lda #>number
            sta itoa_number+1

            lda #<decimal
            sta itoa_string+0
            lda #>decimal
            sta itoa_string+1

            jsr itoa
            ; decimal = "04096" which is screen code representation of a converted value

            jsr itoa_remove_leading_zeroes
            ; Y = number of digits in the output string
            ; decimal = "4096 " which is screen code representation of a left-aligned value without leading zeroes
    ;------------------------
    number  .dsb 2,$00
    decimal .dsb 5,$00
    ;------------------------

Screen code <=> PETSCII conversion
----------------------------------

Including `screen-code-petscii-conversion.src` source code file into your program exposes the following two subroutines for use: `screen_code_to_petscii` and `petscii_to_screen_code`. Each conversion procedure is implemented using only 36 bytes of code and data. Its use is recommended whenever you write a program with critical memory limitations and you need to apply this kind of generalised data conversion.

Example of use:

    #include "screen-code-petscii-conversion.src"

    lda #$01 ; screen code of the letter "A"
    jsr screen_code_to_petscii
    ; A = $41 which is PETSCII for the letter "A"

    lda #$41 ; PETSCII for the letter "A"
    jsr petscii_to_screen_code
    ; A = $01 which is screen code of the letter "A"

There is a little price to pay for a tiny memory foot print of these two subroutines: they will not convert `$ff` input bytes properly.

IRQ routine initialization and setup
------------------------------------

Including `irq-setup.src` source code file into your program exposes a couple of auxiliary subroutines enabling initialization and setup of an IRQ routine that is easy as pie.

    ;------------------------
    #include "irq-setup.src"
    ;------------------------
            ; Initialize IRQ:
            jsr irq_init
    ;------------------------
            ; Initialize soundtrack:
            lda #$00
            jsr $1000
    ;------------------------
            ; Enable IRQ:
            clc
            ; Setup a default IRQ:
            lda #$00
            ldx #<myirq
            ldy #>myirq
            jsr irq_setup
    ;------------------------
            ...
    ;------------------------
            ; Disable IRQ:
            sec
            ; Reset IRQ:
            jsr irq_reset
    ;------------------------
            ...
    ;------------------------
    myirq   ; Play soundtrack:
            inc $d020
            jsr $1003
            dec $d020

            lda #$00
            ldx #<myirq
            ldy #>myirq
            rts
    ;------------------------

Setting a carry flag (`.C`) before calling `irq_setup` will disable IRQ after returning from a subroutine. Clearing a carry flag (`.C`) before calling `irq_setup` will enable IRQ after returning from a subroutine.

VIC macros
----------

This source code file provides a handful of auxiliary helper macros that automatically handle execution of some mundane computations related to VIC setup in your program. Just include it at the top of your source code file and use anywhere:

    ;----------------------------------
    #include "vic.src"
    ;----------------------------------
    CHARSET     = $1000
    SCREEN      = $3c00
    ;----------------------------------
                ; ...
    ;----------------------------------
                lda #.d018(CHARSET,SCREEN)
                sta $d018
                lda $dd00
                and #$fc
                ora #.dd00(CHARSET)
                sta $dd00
    ;----------------------------------

Numbers
-------

The following source code incorporates a set of math related macros to handle miscellaneous operations with numbers.

### Validate if a number value is within a given range (inclusive)

    ;----------------------------------
    #include "numbers.src"
    ;----------------------------------
    TEST_VALUE  = $5f
    LOWER_BOUND = $53
    UPPER_BOUND = $b9
    ;----------------------------------
                .is_number_within_range_incl(TEST_VALUE,LOWER_BOUND,UPPER_BOUND)
                ; carry set = result indicates true result
                ; carry clear = result indicates false result
    ;----------------------------------

Please note that the result logic is an opposite to an exclusive range validation (see a paragraph below), i.e. a number value within a given range yields true by setting the carry flag.

### Validate if a number value is within a given range (exclusive)

    ;----------------------------------
    #include "numbers.src"
    ;----------------------------------
    TEST_VALUE  = $e0
    LOWER_BOUND = $53
    UPPER_BOUND = $b9
    ;----------------------------------
                .is_number_within_range_excl(TEST_VALUE,LOWER_BOUND,UPPER_BOUND)
                ; carry clear = result indicates true result
                ; carry set = result indicates false result
    ;----------------------------------

Please note that the result logic is an opposite to an inclusive range validation (see a paragraph above), i.e. a number value within a given range yields true by clearing the carry flag.

2-byte hexadecimal number to PETSCII conversion
-----------------------------------------------

Including `hex2petscii.src` source code file into your program exposes `hex2petscii` subroutine for use. It converts a 2-byte hexadecimal number referred via `hex2petscii_number` vector to a sequence of _PETSCII_ digits representing its hexadecimal value in a human-readable form and writes them into a memory location identified via `hex2petscii_string` vector (both vectors have to be properly initialized before actually calling a procedure).

This procedure requires 4 bytes of a zero page memory as defined at the top of a source code file in a `_hex2petscii_zp` constant.

Example of use:

    ;------------------------
    #include "hex2petscii.src"
    ;------------------------
    VALUE   = $8a9b
    ;------------------------
            lda #<VALUE
            sta number+0
            lda #>VALUE
            sta number+1

            lda #<number
            sta hex2petscii_number+0
            lda #>number
            sta hex2petscii_number+1

            lda #<hexadecimal
            sta hex2petscii_string+0
            lda #>hexadecimal
            sta hex2petscii_string+1

            jsr hex2petscii
            ; hexadecimal = "8a9b" which is PETSCII representation of a converted value
    ;------------------------
    number      .dsb 2,$00
    hexadecimal .dsb 4,$00
    ;------------------------

3-digit decimal PETSCII string to number conversion
---------------------------------------------------

Including `dec2number.src` source code file into your program exposes `dec2number` subroutine for use. It converts a 3-digit decimal PETSCII string located at `dec2number_digits` memory location to a single-byte number representing its numeric value and writes it back to an accumulator register.

This procedure requires 3 bytes of a zero page memory as defined at the top of a source code file in a `_dec2number_zp` constant.

Example of use:

    ;------------------------------
    #include "dec2number.src"
    ;------------------------------
            ldx #$00
            lda number,x
            sta dec2number_digits,x
            inx
            cpx #$03
            bne *-8

            jsr dec2number
            ; .A = $2a which is numeric value of a converted PETSCII string
    ;------------------------------
    number  .dp "042"
    ;------------------------------

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2015-2021 by Pawel Krol.

This software is distributed under the terms of the MIT license. See [LICENSE](https://bitbucket.org/pawelkrol/commodore-scripts/src/master/LICENSE.md) for more information.
