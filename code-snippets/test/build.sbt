scalaVersion := "2.12.2"

resolvers += "Sonatype Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots/"

libraryDependencies += "com.github.pawelkrol" % "commtest" % "0.02-SNAPSHOT" % "test"

// Define the task key
val compileSrcTask = TaskKey[Unit]("compileSrc", "Compile test sources.")

// Define the task in your projects settings
compileSrcTask <<= compileSrc

// Make test dependent on it
(test in Test) <<= (test in Test) dependsOn (compileSrc)

// Compile test sources
def compileSrc = (streams) map { (s) => {
  s.log.info("Compiling test sources:")
  val src = (new File(".")).listFiles.filter(f => f.isFile && f.getName.endsWith(".src")).toList
  src.foreach(f => {
    val name = f.getName
    val base = name.substring(0, name.length - 4)
    val prg = "target/" + base + ".prg"
    val log = "target/" + base + ".log"
    s.log.info("  - " + name + " -> " + prg + " + " + log)
    val cmd = "    dreamass -I${COMMODORE_SCRIPTS} -me 10 -mw 10 -v -Wall -l " + log + " -o " + prg + " " + name
    s.log.info(cmd)
    val command = cmd.split(" ").filterNot(_.isEmpty).toSeq
    command !
  })
} }
