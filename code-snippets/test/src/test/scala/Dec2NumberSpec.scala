import com.github.pawelkrol.CPU6502.ByteVal

class Dec2NumberSpec extends CodeSnippetsSpec {

  source = "dec2number"

  private def validate(number: Int) {
    val decNumber = "%03d".format(number).split("").map(digit => ByteVal(digit.toInt + 0x30))
    it("converts '%03d' PETSCII string to a numeric value of %d".format(number, number)) {
      writeBytesAt("dec2number_digits", decNumber: _*)
      call
      assert(AC === number)
    }
  }

  describe("dec2number") {
    context("three-digits decimal number conversion") {
      validate(0)
      validate(1)
      validate(9)
      validate(10)
      validate(11)
      validate(19)
      validate(20)
      validate(21)
      validate(99)
      validate(100)
      validate(101)
      validate(199)
      validate(200)
      validate(201)
      validate(254)
      validate(255)
    }
  }
}
