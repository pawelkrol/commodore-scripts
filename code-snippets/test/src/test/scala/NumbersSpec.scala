class NumbersSpec extends CodeSnippetsSpec {

  source = "numbers"

  context("regular range") {
    val lowerBound = 0x53

    val upperBound = 0xb9

    before {
      XR = lowerBound
      YR = upperBound
    }

    describe("is_number_within_inclusive_range") {
      context("when the number is vastly below range") {
        it("carry flag is clear") {
          AC = 0x3f
          call
          assert(CF === false)
        }
      }

      context("when the number is below bottom range") {
        it("carry flag is clear") {
          AC = lowerBound - 1
          call
          assert(CF === false)
        }
      }

      context("when the number is exactly bottom range") {
        it("carry flag is set") {
          AC = lowerBound
          call
          assert(CF === true)
        }
      }

      context("when the number is above bottom range") {
        it("carry flag is set") {
          AC = lowerBound + 1
          call
          assert(CF === true)
        }
      }

      context("when the number is plainly within range") {
        it("carry flag is set") {
          AC = 0x5f
          call
          assert(CF === true)
        }
      }

      context("when the number is below upper range") {
        it("carry flag is set") {
          AC = upperBound - 1
          call
          assert(CF === true)
        }
      }

      context("when the number is exactly upper range") {
        it("carry flag is set") {
          AC = upperBound
          call
          assert(CF === true)
        }
      }

      context("when the number is above upper range") {
        it("carry flag is clear") {
          AC = upperBound + 1
          call
          assert(CF === false)
        }
      }

      context("when the number is vastly above range") {
        it("carry flag is clear") {
          AC = 0xe0
          call
          assert(CF === false)
        }
      }
    }

    describe("is_number_within_exclusive_range") {
      context("when the number is vastly below range") {
        it("carry flag is set") {
          AC = 0x3f
          call
          assert(CF === true)
        }
      }

      context("when the number is below bottom range") {
        it("carry flag is set") {
          AC = lowerBound - 1
          call
          assert(CF === true)
        }
      }

      context("when the number is exactly bottom range") {
        it("carry flag is set") {
          AC = lowerBound
          call
          assert(CF === true)
        }
      }

      context("when the number is above bottom range") {
        it("carry flag is clear") {
          AC = lowerBound + 1
          call
          assert(CF === false)
        }
      }

      context("when the number is plainly within range") {
        it("carry flag is clear") {
          AC = 0x5f
          call
          assert(CF === false)
        }
      }

      context("when the number is below upper range") {
        it("carry flag is clear") {
          AC = upperBound - 1
          call
          assert(CF === false)
        }
      }

      context("when the number is exactly upper range") {
        it("carry flag is set") {
          AC = upperBound
          call
          assert(CF === true)
        }
      }

      context("when the number is above upper range") {
        it("carry flag is set") {
          AC = upperBound + 1
          call
          assert(CF === true)
        }
      }

      context("when the number is vastly above range") {
        it("carry flag is set") {
          AC = 0xe0
          call
          assert(CF === true)
        }
      }
    }
  }

  context("full range") {
    val lowerBound = 0x00

    val upperBound = 0xff

    before {
      XR = lowerBound
      YR = upperBound
    }

    describe("is_number_within_inclusive_range") {
      context("when the number is exactly bottom range") {
        it("carry flag is set") {
          AC = lowerBound
          call
          assert(CF === true)
        }
      }

      context("when the number is above bottom range") {
        it("carry flag is set") {
          AC = lowerBound + 1
          call
          assert(CF === true)
        }
      }

      context("when the number is below upper range") {
        it("carry flag is set") {
          AC = upperBound - 1
          call
          assert(CF === true)
        }
      }

      context("when the number is exactly upper range") {
        it("carry flag is set") {
          AC = upperBound
          call
          assert(CF === true)
        }
      }
    }

    describe("is_number_within_exclusive_range") {
      context("when the number is exactly bottom range") {
        it("carry flag is set") {
          AC = lowerBound
          call
          assert(CF === true)
        }
      }

      context("when the number is above bottom range") {
        it("carry flag is clear") {
          AC = lowerBound + 1
          call
          assert(CF === false)
        }
      }

      context("when the number is below upper range") {
        it("carry flag is clear") {
          AC = upperBound - 1
          call
          assert(CF === false)
        }
      }

      context("when the number is exactly upper range") {
        it("carry flag is set") {
          AC = upperBound
          call
          assert(CF === true)
        }
      }
    }
  }
}
