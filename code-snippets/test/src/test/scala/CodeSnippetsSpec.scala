import com.github.pawelkrol.CommTest.FunSpec

trait CodeSnippetsSpec extends FunSpec {

  private var _source: String = _

  protected def source: String = _source

  protected def source_=(name: String) {
    outputPrg = target(name, "prg")
    labelLog = target(name, "log")
  }

  private def target(name: String, extension: String) =
    "target/" + name + "-test." + extension
}
