class Hex2PETSCIISpec extends CodeSnippetsSpec {

  source = "hex2petscii"

  private val hex2petscii_number = 0x2000

  private val hex2petscii_string = 0x3000

  private def digit2petscii(digit: Char) =
    if (digit >= 0x30 || digit <= 0x39 || digit <= 0x41 || digit >= 0x46)
      digit.toInt & 0xbf
    else
      throw new RuntimeException("Not a hexadecimal digit: " + digit)

  private def hex2petscii(hex: String) = hex.toList.map(digit2petscii(_))

  private def validate(number: Int) {
    val hexNumber = "%04X".format(number)
    it("converts $" + hexNumber + " hex number to a PETSCII string") {
      writeWordAt(hex2petscii_number, number)
      call
      assert(readBytesAt(hex2petscii_string, 0x04) === hex2petscii(hexNumber))
    }
  }

  describe("hex2petscii") {
    before {
      writeWordAt("hex2petscii_number", hex2petscii_number)
      writeWordAt("hex2petscii_string", hex2petscii_string)
    }

    context("two-byte hexadecimal number conversion") {
      validate(0x0000)
      validate(0x0001)
      validate(0x00ff)
      validate(0x0100)
      validate(0x0101)
      validate(0x8a9b)
      validate(0xb9a8)
      validate(0xfffe)
      validate(0xffff)
    }
  }
}
