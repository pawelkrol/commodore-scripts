build-system
============

`build-system` section provides several template files which are meant to relieve the burden of setting up your own build system and letting you focus on the important stuff, that is writing an actual code. It is achieved by providing means of automated compilation of your MOS 6510 assembler source code routines, generating compressed executables and finally running them in the [VICE](http://vice-emu.sourceforge.net/) emulator the way you want it.

Makefile.generic
----------------

This generic `Makefile` is designed to assist you in speeding up your development process by literally automating everything. Once you switch over to using this method developing the software will become a piece of cake. All you have to do is to follow a few conventions.

You obviously need to create a dedicated `Makefile` for each one of your projects, however there is not much you have to type in them:

    PROGRAM=my-new-project

    # X64_EXE=/usr/local/bin/x64sc

    include Makefile.generic

You may structure an entire source code in any way you want, given you comply with the following two rules.

Notice the `PROGRAM` name. It tells `make` where to find the main source code file and where to read its starting address from. So, a file named `my-new-project.inc` must exist in the same directory as your project's `Makefile`. This is where you would normally define all kind of constants used across your program. `START_ADDRESS` has to appear amongst them:

    ;----------------------------------
    START_ADDRESS   = $3000 ; ... $4000
    ;----------------------------------

Your main source code file has to be named `my-new-project.src` and it should begin at a previously defined `START_ADDRESS` (this of course may as well be defined in one of the files included later). Also do not forget to include `my-new-project.inc`
in it, otherwise compilation process will fail:

    ;----------------------------------
    #include "my-new-project.inc"
    ;----------------------------------
                *= START_ADDRESS
    ;----------------------------------
    ; Your code begins here...

And this is it. Your program is now setup and ready to build. To run it in an emulator you only need to type:

    $ make test

Everything else is left entirely up to you and your personal preferences.

### IDE64 Support

This system does not only let you create a [D64](http://ist.uwaterloo.ca/~schepers/formats/D64.TXT) image with an executable version of your program, but also an HDD image which is ready to mount if you enable [IDE64](http://ide64.org/) device emulation. This is again designed to be a piece of cake, thus you simply type `make test-ide64` and you have it all done automatically.

The only target that is required to be defined by your project's `Makefile` is `setup-ide64`. This is the place to configure your HDD's directory structure and to copy all relevant files over to a disk image (please note that `$(PROGRAM_EXE)` shall be copied over by default to the top directory of an IDE64 system drive):

```
setup-ide64:
	mkdir "$(IDE64_SYSTEM_DRIVE)/files"
	cp -v 01 "$(IDE64_SYSTEM_DRIVE)/files/01"
	chmod 777 "$(IDE64_SYSTEM_DRIVE)/files/01,prg";
```

### +60k Support

+60k RAM expansion is supported as well. If you type `make test-plus60k` a compiled program will run in an emulator equipped with a virtual +60k unit.

### Preliminary Setup

There are some hard-coded paths to [Retro Replay](http://rr.c64.org/wiki/Main_Page) and [IDEDOS](http://singularcrew.hu/idedos/) cartridges, since they are attached upon `x64sc` execution. Please make sure to inspect the contents of `Makefile.generic` and adapt it accordingly before using it (those cartridges are likely to be found in different locations on your file system than on mine).

### Compilation parameters

In order to compile a program using an additional option, provide an extra parameter to a build variable named `TEST`:

    $ make test TEST=VAR_NAME

From now on you may refer to such a predefined variable in your program's conditional macros:

    #ifdef TEST_VAR_NAME
    ...
    #else
    ...
    #endif

### Additional prerequisites

In order to provide additional prerequisites, define an extra variable `ADDITIONAL_PREREQUISITES` before `include Makefile.generic`:

    ADDITIONAL_PREREQUISITES := $(F1_EXE) $(F2_EXE)

Any files defined in the `ADDITIONAL_PREREQUISITES` list must be generated as output artifacts of some other build target, as they will be removed by the `clean` target.

### Additional disk files

In order to provide additional disk files to be written into a target disk image, define an extra variable `ADDITIONAL_DISK_FILES` before `include Makefile.generic`:

    ADDITIONAL_DISK_FILES := $(F1_EXE) $(F2_EXE)

Any files defined in the `ADDITIONAL_DISK_FILES` list must be cleaned up manually, as they will **not** be removed by the `clean` target.

### Extending clean target

In order to remove any intermediate build artifacts, define an extra variable `ADDITIONAL_CLEANUP` before `include Makefile.generic`:

    ADDITIONAL_CLEANUP := $(F1_PRG) $(F1_LOG $(F2_PRG) $(F2_LOG))

### Additional build flags

Configure build flags and their values *BEFORE* `include Makefile.generic`:

    SKIP_INTRO := 0|1
    DREAMASS_BUILD_FLAGS := SKIP_INTRO

And that's all, `$(DREAMASS)` command shall be set up accordingly:

    #ifdef SKIP_INTRO
    ...
    #endif

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2015-2023 by Pawel Krol.

This software is distributed under the terms of the MIT license. See [LICENSE](https://bitbucket.org/pawelkrol/commodore-scripts/src/master/LICENSE.md) for more information.
