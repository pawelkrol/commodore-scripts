#!/usr/bin/perl
#
# perl generate-synchronisation-macros.pl --max-cycles=<NUMBER>

use strict;
use warnings;

use Data::Dumper;
use Getopt::Long;
use List::Util qw(max sum);
use Readonly;

Readonly my $DEFAULT_MAX_CYCLES => 50;
Readonly my $INDENTATION => 16;

my ($max_cycles) = get_options();
my @source_code = generate_source_code($max_cycles);
print_out_source_code(@source_code);

sub get_options {
  my $max_cycles = $DEFAULT_MAX_CYCLES;
  GetOptions('max-cycles=i' => \$max_cycles);

  die '"--max-cycles" must be a natural number' if $max_cycles !~ m/^\d+$/;

  return ($max_cycles);
}

sub generate_source_code {
  my ($max_cycles) = @_;

  my @source_code;
  for my $num_cycles (2 .. $max_cycles) {

    my ($num_loops, $source_code) = generate_cycle_code($num_cycles);

    my $macro_name = sprintf 'wait_for_%d_cycles', $num_cycles;
    push @source_code, {
      macro_name  => $macro_name,
      num_loops   => $num_loops,
      source_code => $source_code,
    };
  }

  return @source_code;
}

sub generate_cycle_code {
  my ($num_cycles) = @_;

  my ($num_loops, @parts) = split_num_cycles_into_parts($num_cycles);

  my $source_code = build_cycle_code_from_parts(@parts);

  return ($num_loops, $source_code);
}

sub split_num_cycles_into_parts {
  my ($num_cycles) = @_;

  my @source_config = get_source_config();

  # Get the shortest possible combination adding up to the number of cycles:
  my $combination = get_the_shortest_combination($num_cycles, @source_config);

  # Keep the number of loop iterations for further print formatting:
  my $num_loops = $combination->[0] - 1;

  # Configure parts and number of their occurrences together which correspond
  # to current number of cycles:
  my @parts;
  for (my $i = 0; $i < @source_config; $i++) {
    my $num_occurrences = shift @{$combination};
    last unless defined $num_occurrences;

    if ($num_occurrences > 0) {
      push @parts, { config => $source_config[$i], num_occurrences => $num_occurrences };
    }
  }

  return ($num_loops, @parts);
}

sub get_source_config {
  my $source_code_6_plus_5_cycles = <<SOURCE_CODE;
    ldx #\$<NUM_ITER> ; 2 cycles
    dex      ;
    bne *-1  ; 2 + 3 cycles -> 5 cycles * <NUM_ITER_MIN_ONE> + 4 cycles
SOURCE_CODE

  my $source_code_3_plus_3_cycles = <<SOURCE_CODE;
    bit \$ea  ; 3 cycles
SOURCE_CODE

  my $source_code_2_plus_2_cycles = <<SOURCE_CODE;
    nop      ; 2 cycles
SOURCE_CODE

  my @source_config = (
    {
      first_cycle => 6,
      next_cycles => 5,
      source_code => $source_code_6_plus_5_cycles,
    },
    {
      first_cycle => 3,
      next_cycles => 3,
      source_code => $source_code_3_plus_3_cycles,
    },
    {
      first_cycle => 2,
      next_cycles => 2,
      source_code => $source_code_2_plus_2_cycles,
    },
  );

return @source_config;
}

sub get_the_shortest_combination {
  my ($num_cycles, @source_config) = @_;

  # Build all combinations that build up exactly to a given number of cycles:
  my @combinations = build_possible_combinations($num_cycles, [], @source_config);

  # Pick up the shortest combination out of all the possibilities:
  @combinations = sort {
    my $num_loops_a = $a->[0] || 0;
    my $num_loops_b = $b->[0] || 0;
    my $num_components_a = sum @$a;
    my $num_components_b = sum @$b;
    $num_loops_b <=> $num_loops_a || $num_components_a <=> $num_components_b;
  } @combinations;

  return $combinations[0];
}

sub build_possible_combinations {
  my ($num_cycles, $components_so_far, @source_config) = @_;

  return () unless @source_config;

  my $value_from_components_so_far = calculate_total_value_from_components($components_so_far);

  my $component_first_cycle = $source_config[0]->{first_cycle};
  my $component_next_cycles = $source_config[0]->{next_cycles};

  splice @source_config, 0, 1;

  my @final_combinations;

  # Pick up current component and iterate through its all fitting values:
  for (my $i = 0;; $i++) {
    my $added_value = 0;
    $added_value += $component_first_cycle if $i > 0;
    $added_value += $component_next_cycles * ($i - 1) if $i > 1;

    my $value = $value_from_components_so_far + $added_value;

    last if $value > $num_cycles;

    if ($value == $num_cycles) {
      my $combination = build_new_combination($components_so_far, $i);
      push @final_combinations, $combination;
    }
    else {
      my @deep_components_so_far = (@{$components_so_far}, $i);
      my @additional_combinations = build_possible_combinations($num_cycles, \@deep_components_so_far, @source_config);
      push @final_combinations, @additional_combinations;
    }
  }

  return @final_combinations;
}

sub calculate_total_value_from_components {
  my ($components_so_far) = @_;

  my @source_config = get_source_config();

  my $total_value = 0;

  for (my $i = 0; $i < @source_config; $i++) {

    my $component_config = $source_config[$i];
    my $component_count = $components_so_far->[$i] || 0;

    if ($component_count > 0) {
      $total_value += $component_config->{first_cycle};
      $component_count--;
    }

    if ($component_count > 0) {
      $total_value += $component_config->{next_cycles} * $component_count;
    }
  }

  return $total_value;
}

sub build_new_combination {
  my ($previous_components, $current_component) = @_;

  my @combination = (@{$previous_components}, $current_component);

  return \@combination;
}

sub build_cycle_code_from_parts {
  my (@parts) = @_;

  my $source_code;

  for my $part (@parts) {
    my $config = $part->{config};
    my $num_occurrences = $part->{num_occurrences};

    my $template_code = $config->{source_code};

    if ($template_code =~ m/<NUM_ITER>/) {
      my $num_iterations = sprintf '%02x', $num_occurrences;
      my $num_iterations_minus_one = $num_occurrences - 1;

      $template_code =~ s/<NUM_ITER>/$num_iterations/;
      $template_code =~ s/<NUM_ITER_MIN_ONE>/$num_iterations_minus_one/;
      $source_code .= $template_code;
    }
    else {
      for (1 .. $num_occurrences) {
        $source_code .= $template_code;
      }
    }
  }

  return $source_code;
}

sub print_out_source_code {
  my (@source_code) = @_;

  #  Find maximum possible length of number of displayed loop iterations:
  my $max_num_loops_length = length max map { $_->{num_loops} } @source_code;

  my $comment_line = sprintf ";%s\n", '-' x ($INDENTATION + 48 + $max_num_loops_length);

  for my $cycle (@source_code) {
    my $macro_name = $cycle->{macro_name};
    my $source_code = $cycle->{source_code};

    $source_code =~ s/^\s+/' ' x $INDENTATION/meg;

    print $comment_line;
    printf "#macro ${macro_name}()\n{\n";
    print $source_code;
    printf "}\n";
  }

  print $comment_line;

  return;
}
