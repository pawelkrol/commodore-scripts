code-generators
===============

`code-generators` section provides several useful [Perl](http://www.perl.org/) scripts that can generate assembler source code routines and macros for a family of MOS 6510 processors.

generate-screen-source-code.pl
------------------------------

This script generates and prints out a source code consisting of a single screen text data of 40x25 characters as provided in an input text file. It validates all included ASCII characters against the PETSCII character set and (depending on a selected character set: `shifted` vs `unshifted`) either outputs a source code with all characters converted to their respective screen codes or reports back an error with encountering an invalid PETSCII character.

Examples of usage:

    $ perl generate-screen-source-code.pl --screen-text=info-screen-01.txt --charset=shifted
    $ perl generate-screen-source-code.pl --screen-text=info-screen-02.txt --charset=unshifted

Generated source files may be included into your program's source code just like any other included source file:

    #include "screen-text=info-screen-01.src"

There are several ASCII code sequences, extending the basic functionality, available:

* Typing `{$63}` lets you specify a single screen code to be passed through to the output.
* Typing `{!dup($63,40)}` lets you specify more identical screen codes to be passed through to the output by a given number of times.

Lines are not wrapped, thus any content longer than 40 characters will be dropped.

generate-synchronisation-macros.pl
----------------------------------

This script generates and prints out a source code consisting of a desirable number of synchronisation macros with a step of 1 cycle up to a requested maximum number of cycles (which normally won't be bigger than several dozen, as you probably don't want to waste too many CPU cycles doing nothing, do you?). They operate under an assumption that no page boundary is ever crossed on branching during delay loop execution (`bne` instruction will get penalty cycles every time high byte of a destination address needs to be modified).

Examples of usage:

    $ perl generate-synchronisation-macros.pl
    $ perl generate-synchronisation-macros.pl --max-cycles=<NUMBER>

When you save generated source code to an include file, you can call any established macro from within your program using the following preprocessor directive (replace `XX` with a desired number of CPU cycles):

    .wait_for_XX_cycles()

COPYRIGHT AND LICENCE
---------------------

Copyright (C) 2014, 2015 by Pawel Krol.

This library is free open source software; you can redistribute it and/or modify it under the same terms as Perl itself, either Perl version 5.8.6 or, at your option, any later version of Perl 5 you may have available.

PLEASE NOTE THAT IT COMES WITHOUT A WARRANTY OF ANY KIND!