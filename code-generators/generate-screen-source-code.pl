#!/usr/bin/perl
#
# perl generate-screen-source-code.pl --screen-text=<FILENAME> --charset=[shifted|unshifted]

use strict;
use warnings;

use Data::Dumper;
use File::Slurp;
use Getopt::Long;
use Readonly;

Readonly our %screen_codes => (
  shifted   => [
    undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, # $00..$0f
    undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, # $10..$1f
    0x20,  0x21,  0x22,  0x23,  0x24,  0x25,  0x26,  0x27,  0x28,  0x29,  0x2a,  0x2b,  0x2c,  0x2d,  0x2e,  0x2f,  # $20..$2f
    0x30,  0x31,  0x32,  0x33,  0x34,  0x35,  0x36,  0x37,  0x38,  0x39,  0x3a,  0x3b,  0x3c,  0x3d,  0x3e,  0x3f,  # $30..$3f
    0x00,  0x41,  0x42,  0x43,  0x44,  0x45,  0x46,  0x47,  0x48,  0x49,  0x4a,  0x4b,  0x4c,  0x4d,  0x4e,  0x4f,  # $40..$4f
    0x50,  0x51,  0x52,  0x53,  0x54,  0x55,  0x56,  0x57,  0x58,  0x59,  0x5a,  0x1b,  undef, 0x1d,  undef, 0x64,  # $50..$5f
    undef, 0x01,  0x02,  0x03,  0x04,  0x05,  0x06,  0x07,  0x08,  0x09,  0x0a,  0x0b,  0x0c,  0x0d,  0x0e,  0x0f,  # $60..$6f
    0x10,  0x11,  0x12,  0x13,  0x14,  0x15,  0x16,  0x17,  0x18,  0x19,  0x1a,  undef, undef, undef, undef, undef, # $70..$7f
  ],
  unshifted => [
    undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, # $00..$0f
    undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, undef, # $10..$1f
    0x20,  0x21,  0x22,  0x23,  0x24,  0x25,  0x26,  0x27,  0x28,  0x29,  0x2a,  0x2b,  0x2c,  0x2d,  0x2e,  0x2f,  # $20..$2f
    0x30,  0x31,  0x32,  0x33,  0x34,  0x35,  0x36,  0x37,  0x38,  0x39,  0x3a,  0x3b,  0x3c,  0x3d,  0x3e,  0x3f,  # $30..$3f
    0x00,  0x01,  0x02,  0x03,  0x04,  0x05,  0x06,  0x07,  0x08,  0x09,  0x0a,  0x0b,  0x0c,  0x0d,  0x0e,  0x0f,  # $40..$4f
    0x10,  0x11,  0x12,  0x13,  0x14,  0x15,  0x16,  0x17,  0x18,  0x19,  0x1a,  0x1b,  undef, 0x1d,  undef, 0x64,  # $50..$5f
    undef, 0x01,  0x02,  0x03,  0x04,  0x05,  0x06,  0x07,  0x08,  0x09,  0x0a,  0x0b,  0x0c,  0x0d,  0x0e,  0x0f,  # $60..$6f
    0x10,  0x11,  0x12,  0x13,  0x14,  0x15,  0x16,  0x17,  0x18,  0x19,  0x1a,  undef, undef, undef, undef, undef, # $70..$7f
  ],
);

my ($screen_text, $charset) = get_options();
my @lines = split /\n/, read_file $screen_text;
print_out_screen_codes($charset, @lines);

sub get_options {
  my $screen_text = '';
  my $charset = 'unshifted';

  GetOptions('screen-text=s' => \$screen_text, 'charset=s' => \$charset);

  die '"--screen-text" file must exist' unless -e $screen_text;
  die '"--charset" must be defined' unless defined $charset;

  return ($screen_text, $charset);
}

sub print_out_screen_codes {
  my ($charset, @lines) = @_;
  for (my $i = 0x00; $i < 0x19; $i++) {
    print '.ds ';
    for (my $j = 0x00; $j < 0x28; $j++) {
      no warnings 'substr';
      my $byte = defined ($lines[$i]) ? substr ($lines[$i], $j, 0x01) : ' ';
      my $ascii = defined ($byte) ? ord ($byte) : 0x20;
      $ascii = 0x20 unless $ascii;
      if ($ascii == 0x7b) { # == '{'
        my ($skip, $forward) = process_special($lines[$i], $j);
        substr $lines[$i], $j, $skip, '';
        $j += $forward;
      }
      else {
        my $ascii_code = sprintf '$%02x', $ascii;
        my $screen_code = $screen_codes{$charset}->[$ascii];
        my $char = chr $ascii;
        die "Unsupported ASCII character: '${char}' (${ascii_code})" unless defined $screen_code;
        printf '$%02x', $screen_code;
      }
      print ',' if $j < 0x27;
    }
    print "\n";
  }
}

sub process_special {
  my ($line, $index) = @_;
  my ($cmd) = substr ($line, $index) =~ m/^\{([^}]*)\}/;
  my $skip = length ($cmd) + 1;
  my $forward = 0;
  if ($cmd =~ m/^!(.+)\((.+)\)$/) {
    my $func = $1;
    my $args = $2;
    $forward += eval "$func('$args')";
  }
  else {
    die "Unknown macro: '${cmd}'" unless $cmd =~ m/^\$[0-9a-f]{2}$/i;
    print $cmd;
  }
  return ($skip, $forward);
}

sub dup {
  my ($args) = @_;
  die "Invalid macro: '!dup(${args})'" unless $args =~ m/^(\$[0-9a-f]{2}),(\d+)$/i;
  my $value = $1;
  my $count = $2 - 1;
  print join ',', map { $value } (0 .. $count);
  return $count;
}